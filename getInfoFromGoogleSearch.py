#python3
'''
Description: Utilizes pyautogui to screen scrape google search results. Also detects captcha and pauses until resolved (manually)
Demo: https://youtu.be/IPDWMWX3-ZM

pre-reqs...
#based on ubuntu based linux system (linux mint tested)
sudo apt-get install scrot python3-tk python3-dev
pip3 install python3-xlib pyautogui pyperclip
one of the following (your choice) - https://pyperclip.readthedocs.io/en/latest/introduction.html#not-implemented-error
    sudo apt-get install xsel
    sudo apt-get install xclip

'''

import pyautogui
import time
import sys
import pyperclip
import re

#global constants
SEC_TO_GIVE_USER_TO_PERFORM_ACTION_DEFAULT = 1
SEC_TO_GIVE_BROWSER_TO_LOAD_DEFAULT = 1
SEC_BETWEEN_COMMANDS_DEFAULT = 0.1
NUM_OF_TIMES_TO_SHIFT_TAB_BACKWARDS_TO_NEXT = 8  # need to make sure this lands on Next at the bottom



def getQueryString(searchBox_xPos, searchBox_yPos):
    pyautogui.click(x=searchBox_xPos, y=searchBox_yPos, clicks=1, interval=1, button='left')
    pyautogui.hotkey('ctrl', 'a')
    pyautogui.hotkey('ctrl', 'c')
    clipboardText = pyperclip.paste() # read clipboard text   
    return clipboardText.strip()
    
    

def getRegexParser():
    print("Define a regex to use to extract data. Enter on of the keywords to use a canned response, or enter your own. Be sure to include the capture () characters.")
    print("\tEMAIL: ([_a-z0-9-]+(?:\.[_a-z0-9-]+)*@[a-z0-9-]+(?:\.[a-z0-9-]+)*(?:\.[a-z]{2,4}))")
    regexpStr = input("regexp or keyword? ")
    if regexpStr == "EMAIL":
        return "([_a-z0-9-]+(?:\.[_a-z0-9-]+)*@[a-z0-9-]+(?:\.[a-z0-9-]+)*(?:\.[a-z]{2,4}))"
    return regexpStr.strip()

def areThereMoreResults(content):
    return bool(re.search("^Next$", content, flags=re.MULTILINE))


def popUpMessage(theMessage):
    buttonPressed = pyautogui.confirm(text=theMessage+" - Click OK to continue, or Cancel to kill this program.", title='Action Is Needed', buttons=['OK', 'Cancel'])
    if buttonPressed == "Cancel":
        sys.exit()
    return


def checkForCaptcha(clipboardText):
    if bool(re.search("Our systems have detected unusual traffic from your computer", clipboardText, flags=re.MULTILINE)):
        popUpMessage("Ruh roh, a captcha activated. Before clicking Okay, be sure the results window is showing again.")
        return getPageText(resWindowMiddleRight_xPos, resWindowMiddleRight_yPos)
    return clipboardText


def getPageText(resWindowMiddleRight_xPos, resWindowMiddleRight_yPos):
    #grab the data for the page...
    pyautogui.click(x=resWindowMiddleRight_xPos, y=resWindowMiddleRight_yPos, clicks=1, interval=1, button='left')
    pyautogui.hotkey('ctrl', 'a')
    pyautogui.hotkey('ctrl', 'c')
    clipboardText = pyperclip.paste()
    #print(clipboardText)
    return clipboardText



#screenWidth, screenHeight = pyautogui.size()
#print("screenWidth: %s, screenHeight: %s" % (screenWidth, screenHeight))

print("This works best on 2+ monitors. This terminal window on one, your browser in the other.")

# run the query
popUpMessage("First, pull up your browser and submit the query you are interested in automating. Ensure you have the results you are looking for.")

# get location of address bar
popUpMessage('Let\'s grab the location of the address bar in a new tab in your browser. After you click okay, you will have %s seconds to move your cursor to the center of the address bar in your browser. Hold it there until you are told you can move it. Ready?' % SEC_TO_GIVE_USER_TO_PERFORM_ACTION_DEFAULT)

time.sleep(SEC_TO_GIVE_USER_TO_PERFORM_ACTION_DEFAULT)
addressBar_xPos, addressBar_yPos = pyautogui.position()
#print(addressBar_xPos, addressBar_yPos)

popUpMessage('Okay, got it.')

# get location of search box
popUpMessage('Let\'s grab the location of the search box on the Google results page. After you click okay, you will have %s seconds to move your cursor to the center of the search box in Google. Hold it there until you are told you can move it. Ready?' % SEC_TO_GIVE_USER_TO_PERFORM_ACTION_DEFAULT)

time.sleep(SEC_TO_GIVE_USER_TO_PERFORM_ACTION_DEFAULT)
searchBox_xPos, searchBox_yPos = pyautogui.position()
#print(searchBox_xPos, searchBox_yPos)

popUpMessage('Okay, got it.')

# get location of results window
popUpMessage('Now you will show the location of the white space to the left of the results list. After you click okay, you will have %s seconds to move your cursor to the middle/right (a little left of the scroll bar) of your browser. Hold it there until you are told you can move it. Ready?' % SEC_TO_GIVE_USER_TO_PERFORM_ACTION_DEFAULT)

time.sleep(SEC_TO_GIVE_USER_TO_PERFORM_ACTION_DEFAULT)
resWindowMiddleRight_xPos, resWindowMiddleRight_yPos = pyautogui.position()
#print(resWindowMiddleRight_xPos, resWindowMiddleRight_yPos)

popUpMessage('Okay, got it.')


# get query string (from earlier...should still be in the window
queryStr = getQueryString(searchBox_xPos, searchBox_yPos)
print("queryStr: %s" % queryStr)

regexpStr = getRegexParser()
print("regexpStr: %s" % regexpStr)

#run the search...
#pyautogui.click(x=addressBar_xPos, y=addressBar_yPos, clicks=1, interval=1, button='left')
#pyautogui.hotkey('ctrl', 'a')
#pyautogui.hotkey('ctrl', 'c')
#clipboardText = pyperclip.paste() # read clipboard text
#print(clipboardText)
#pyperclip.copy(queryStr) # copy this new string into clipboard
#pyautogui.hotkey('ctrl', 'v')
#pyautogui.hotkey('enter')

while True:

    clipboardText = getPageText(resWindowMiddleRight_xPos, resWindowMiddleRight_yPos)

    clipboardText = checkForCaptcha(clipboardText)

    for match in re.findall( regexpStr, clipboardText, flags=re.IGNORECASE):
        print(match)

    #check to see if there are more results
    if not areThereMoreResults(clipboardText):
        print("no more results...we're done")
        sys.exit()
  
    pyautogui.click(x=addressBar_xPos, y=addressBar_yPos, clicks=1, interval=1, button='left')
    for _ in range(NUM_OF_TIMES_TO_SHIFT_TAB_BACKWARDS_TO_NEXT):
        pyautogui.hotkey('shiftleft', 'tab')
    #pyautogui.click(x=resWindowMiddleRight_xPos, y=resWindowMiddleRight_yPos, clicks=1, interval=1, button='left')
    #for _ in range(5):
    #    pyautogui.hotkey('pagedown')
    #time.sleep(30)        
    pyautogui.hotkey('enter')

    time.sleep(SEC_TO_GIVE_BROWSER_TO_LOAD_DEFAULT)
    #sys.exit()


